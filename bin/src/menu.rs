use crate::{GameArea, Player};
use gio::prelude::*;
use gtk::{
    prelude::*, Adjustment, Align, ApplicationWindow, Box, Button, ButtonsType, DialogFlags, Label,
    MessageDialog, MessageType, Orientation, RadioButton, ResponseType, SpinButton, Widget,
};
use lib::Color;
use std::{cell::RefCell, rc::Rc, time::Duration};

#[derive(Debug)]
pub struct Menu {
    data: Rc<RefCell<Data>>,
    widget: Box,
}

impl Menu {
    pub fn new(window: Rc<ApplicationWindow>, game_area: Rc<GameArea>) -> Self {
        Self {
            data: Rc::new(RefCell::new(Data {
                players: [Player::Human; 2],
                use_restrictive_rule_set: false,
                window,
                game_area,
            })),
            widget: Box::new(Orientation::Vertical, 20),
        }
        .init()
    }

    fn init(self) -> Self {
        self.widget.set_margin_start(10);
        self.widget.set_margin_end(10);
        self.widget.set_margin_top(10);
        self.widget.set_margin_bottom(10);
        self.add_new_game();
        self.add_player(Color::Red);
        self.add_player(Color::Black);
        self.add_rule_set();
        self.add_divider();
        self.add_undo_action();
        self.add_center_game();
        self
    }

    fn add_new_game(&self) {
        let new_game = Button::with_label("New Game");
        let data = Rc::clone(&self.data);
        new_game.connect_clicked(move |_| {
            if data.borrow().game_area.has_started() {
                let dialog = MessageDialog::new(
                    Some(data.borrow().window.as_ref()),
                    DialogFlags::MODAL,
                    MessageType::Warning,
                    ButtonsType::OkCancel,
                    "Are you sure you want to start a new game? The current game state will be lost!",
                );
                let data = Rc::clone(&data);
                dialog.connect_response(move |dialog, response| {
                    if response == ResponseType::Ok {
                        let data = data.borrow();
                        data.game_area.new_game(data.players, data.use_restrictive_rule_set);
                    }
                    dialog.close();
                });
                dialog.show();
            } else {
                let data = data.borrow();
                data.game_area.new_game(data.players, data.use_restrictive_rule_set);
            }
        });
        self.widget.add(&new_game);
    }

    fn add_player(&self, color: Color) {
        let player = Box::new(Orientation::Vertical, 2);
        self.widget.add(&player);

        let label = Label::new(Some(match color {
            Color::Red => "Red Player",
            Color::Black => "Black Player",
        }));
        label.set_halign(Align::Start);
        label.get_style_context().add_class("button-group-header");
        player.add(&label);

        let human = RadioButton::with_label("Human");
        let computer = RadioButton::with_label("Computer");
        computer.join_group(Some(&human));
        player.add(&human);
        player.add(&computer);

        let label = Label::new(Some("Turn time (s)"));
        label.set_margin_top(5);
        label.set_halign(Align::Start);
        player.add(&label);

        let adjustment = Adjustment::new(1.0, 0.01, 999.99, 0.01, 0.1, 0.0);
        let turn_time = Rc::new(SpinButton::new(Some(&adjustment), 0.0, 2));
        turn_time.set_margin_top(5);
        turn_time.set_numeric(true);
        turn_time.set_sensitive(false);
        player.add(turn_time.as_ref());

        self.connect_human_clicked(&human, &turn_time, color);
        self.connect_computer_clicked(&computer, &turn_time, color);
        self.connect_turn_time_value_changed(&turn_time, color);
    }

    fn connect_human_clicked(&self, human: &RadioButton, turn_time: &Rc<SpinButton>, color: Color) {
        let data = Rc::clone(&self.data);
        let turn_time = Rc::clone(turn_time);
        human.connect_clicked(move |_| {
            data.borrow_mut().players[color as usize] = Player::Human;
            turn_time.set_sensitive(false);
        });
    }

    fn connect_computer_clicked(
        &self, computer: &RadioButton, turn_time: &Rc<SpinButton>, color: Color,
    ) {
        let data = Rc::clone(&self.data);
        let turn_time = Rc::clone(turn_time);
        computer.connect_clicked(move |_| {
            data.borrow_mut().players[color as usize] =
                Player::Computer(Duration::from_secs_f64(turn_time.get_value()));
            turn_time.set_sensitive(true);
        });
    }

    fn connect_turn_time_value_changed(&self, turn_time: &Rc<SpinButton>, color: Color) {
        let data = Rc::clone(&self.data);
        turn_time.connect_value_changed(move |turn_time| {
            data.borrow_mut().players[color as usize] =
                Player::Computer(Duration::from_secs_f64(turn_time.get_value()));
        });
    }

    fn add_rule_set(&self) {
        let rule_set = Box::new(Orientation::Vertical, 2);
        self.widget.add(&rule_set);

        let label = Label::new(Some("Rule Set"));
        label.set_halign(Align::Start);
        label.get_style_context().add_class("button-group-header");
        rule_set.add(&label);

        let open = RadioButton::with_label("Open");
        let restrictive = RadioButton::with_label("Restrictive");
        restrictive.join_group(Some(&open));
        rule_set.add(&open);
        rule_set.add(&restrictive);

        let data = Rc::clone(&self.data);
        open.connect_clicked(move |_| {
            data.borrow_mut().use_restrictive_rule_set = false;
        });
        let data = Rc::clone(&self.data);
        restrictive.connect_clicked(move |_| {
            data.borrow_mut().use_restrictive_rule_set = true;
        });
    }

    fn add_divider(&self) {
        let divider = Box::new(Orientation::Vertical, 0);
        divider.set_vexpand(true);
        self.widget.add(&divider);
    }

    fn add_undo_action(&self) {
        let undo_action = Rc::new(Button::with_label("Undo Action"));
        undo_action.set_sensitive(false);
        let data = Rc::clone(&self.data);
        undo_action.connect_clicked(move |_| {
            data.borrow().game_area.undo_action();
        });
        let game_area = &self.data.borrow().game_area;
        game_area.widget().connect_draw({
            let undo = Rc::clone(&undo_action);
            let game_area = Rc::clone(game_area);
            move |_, _| {
                undo.set_sensitive(game_area.undo_available());
                Inhibit(false)
            }
        });
        self.widget.add(undo_action.as_ref());
    }

    fn add_center_game(&self) {
        let center_game = Button::with_label("Center Game");
        let data = Rc::clone(&self.data);
        center_game.connect_clicked(move |_| {
            data.borrow().game_area.center_game();
        });
        self.widget.add(&center_game);
    }

    pub fn widget(&self) -> &impl IsA<Widget> {
        &self.widget
    }
}

#[derive(Debug)]
struct Data {
    players: [Player; 2],
    use_restrictive_rule_set: bool,
    window: Rc<ApplicationWindow>,
    game_area: Rc<GameArea>,
}
