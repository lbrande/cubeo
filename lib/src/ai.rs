use crate::{Action, Color, Die, Game, Pos, MAX_NCONSECUTIVE_MOVES};
use rand::prelude::*;
use rustc_hash::FxHashMap;
use smallvec::SmallVec;

#[derive(Debug)]
pub struct Minimax {
    tt: FxHashMap<u64, Ttvalue>,
    zobrist: FxHashMap<Zobrist, u64>,
    rng: ThreadRng,
    game: Game,
    hash: u64,
}

impl Default for Minimax {
    fn default() -> Self {
        Self::new()
    }
}

impl Minimax {
    pub fn new() -> Self {
        Self {
            tt: FxHashMap::default(),
            zobrist: FxHashMap::default(),
            rng: thread_rng(),
            game: Game::new(),
            hash: 0,
        }
        .init()
    }

    fn init(mut self) -> Self {
        self.generate_hash();
        self
    }

    pub fn action(&mut self, game: Game, depth: u32) -> Action {
        self.prepare_search(game);
        for depth in 1..=depth {
            self.search(depth, -i32::MAX, i32::MAX);
            if self.is_finished() {
                break;
            }
        }
        self.root_action()
    }

    pub fn prepare_search(&mut self, game: Game) {
        self.game = game;
        self.generate_hash();
    }

    pub fn search(&mut self, depth: u32, mut alpha: i32, mut beta: i32) -> i32 {
        let alpha_origin = alpha;
        let mut value = i32::MIN;
        let mut action = Action::None;
        if let Some(value) = self.search_tt(depth, &mut alpha, &mut beta, &mut value, &mut action) {
            return value;
        }
        if self.game.winner.is_none() {
            self.game.generate_actions();
            self.game.winner = self.game.actions.is_empty().then(|| !self.game.turn);
        }
        if self.game.winner.is_some() {
            let value = -i32::MAX;
            self.tt_set(Ttvalue::new(depth, value, Action::None, Ttbound::Exact));
            return value;
        } else if depth == 0 {
            let value = heuristic(&mut self.game);
            self.tt_set(Ttvalue::new(depth, value, Action::None, Ttbound::Exact));
            return value;
        }
        self.search_children(depth, alpha_origin, alpha, beta, value, action)
    }

    pub fn root_action(&self) -> Action {
        self.tt_get().map(|tt_value| tt_value.action).unwrap_or(Action::None)
    }

    pub fn is_finished(&self) -> bool {
        self.tt_get().map(|tt_value| tt_value.depth == u32::MAX).unwrap_or(false)
    }

    fn search_tt(
        &mut self, depth: u32, alpha: &mut i32, beta: &mut i32, value: &mut i32,
        action: &mut Action,
    ) -> Option<i32> {
        if let Some(tt_value) = self.tt_get() {
            if tt_value.depth >= depth {
                match tt_value.bound {
                    Ttbound::Lower => *alpha = (*alpha).max(tt_value.value),
                    Ttbound::Exact => return Some(tt_value.value),
                    Ttbound::Upper => *beta = (*beta).min(tt_value.value),
                }
                if alpha >= beta {
                    return Some(tt_value.value);
                }
            }
            if depth > 0 && tt_value.action != Action::None {
                *action = tt_value.action;
                self.perform_action(*action);
                *value = -self.search(depth - 1, -*beta, -*alpha);
                self.undo_action();
                *alpha = *alpha.max(value);
                if alpha >= beta {
                    self.tt_set(Ttvalue::new(depth, *value, *action, Ttbound::Lower));
                    return Some(*value);
                }
            }
        }
        None
    }

    fn search_children(
        &mut self, depth: u32, alpha_origin: i32, mut alpha: i32, beta: i32, mut value: i32,
        mut action: Action,
    ) -> i32 {
        for child in self.ordered_actions(action) {
            self.perform_action(child);
            let mut child_value = -self.search(depth - 1, -alpha - 1, -alpha);
            if child_value > alpha && child_value < beta {
                child_value = -self.search(depth - 1, -beta, -child_value);
            }
            self.undo_action();
            if child_value > value {
                value = child_value;
                action = child;
                alpha = alpha.max(value);
                if alpha >= beta {
                    break;
                }
            }
        }
        let bound = Ttbound::from_alpha_beta(value, alpha_origin, beta);
        self.tt_set(Ttvalue::new(depth, value, action, bound));
        value
    }

    fn ordered_actions(&mut self, killer: Action) -> SmallVec<[Action; 16]> {
        let mut actions_with_values: SmallVec<[_; 16]> =
            SmallVec::with_capacity(self.game.actions.len());
        for action in self.game.actions.clone() {
            if action != killer {
                let hash = self.hash ^ self.action_hash(action);
                let value = self.tt.get(&hash).map(|tt_value| tt_value.value).unwrap_or(i32::MAX);
                actions_with_values.push((action, value));
            }
        }
        actions_with_values.sort_unstable_by(|a, b| a.1.cmp(&b.1));
        actions_with_values.iter().map(|action| action.0).collect()
    }

    fn perform_action(&mut self, action: Action) {
        self.hash ^= self.action_hash(action);
        self.game.actions.clear();
        self.game.winner = action.perform(&mut self.game);
    }

    fn undo_action(&mut self) {
        let action = self.game.undo_action().unwrap();
        self.hash ^= self.action_hash(action);
    }

    fn tt_get(&self) -> Option<Ttvalue> {
        self.tt.get(&self.hash).copied()
    }

    fn tt_set(&mut self, tt_value: Ttvalue) {
        self.tt.insert(self.hash, tt_value);
    }

    fn generate_hash(&mut self) {
        self.hash = Zobrist::Color(self.game.turn).hash(self);
        if self.game.nconsecutive_moves() < MAX_NCONSECUTIVE_MOVES {
            self.hash ^= Zobrist::MoveIsLegal.hash(self);
        }
        for (pos, die) in self.game.board.clone() {
            self.hash ^= Zobrist::Die(pos, die).hash(self);
        }
    }

    fn action_hash(&mut self, action: Action) -> u64 {
        let mut hash = Zobrist::Color(Color::Red).hash(self);
        hash ^= Zobrist::Color(Color::Black).hash(self);
        match action {
            Action::Add(pos) => {
                if self.game.nconsecutive_moves() == MAX_NCONSECUTIVE_MOVES {
                    hash ^= Zobrist::MoveIsLegal.hash(self);
                }
                hash ^= Zobrist::Die(pos, Die::with_color(self.game.turn)).hash(self);
            }
            Action::Merge(from, to, from_value) => {
                if self.game.nconsecutive_moves() == MAX_NCONSECUTIVE_MOVES {
                    hash ^= Zobrist::MoveIsLegal.hash(self);
                }
                let from_die = self.game.board[&from];
                let to_die = self.game.board[&to];
                let new_die = Die { color: self.game.turn, value: from_value + to_die.value };
                hash ^= Zobrist::Die(from, from_die).hash(self);
                hash ^= Zobrist::Die(to, to_die).hash(self);
                hash ^= Zobrist::Die(to, new_die).hash(self);
            }
            Action::Move(from, to) => {
                if self.game.nconsecutive_moves() + 1 == MAX_NCONSECUTIVE_MOVES {
                    hash ^= Zobrist::MoveIsLegal.hash(self);
                }
                let die = self.game.board[&from];
                hash ^= Zobrist::Die(from, die).hash(self);
                hash ^= Zobrist::Die(to, die).hash(self);
            }
            Action::None => {}
        }
        hash
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
enum Zobrist {
    Die(Pos, Die),
    Color(Color),
    MoveIsLegal,
}

impl Zobrist {
    fn hash(self, minimax: &mut Minimax) -> u64 {
        let rng = &mut minimax.rng;
        *minimax.zobrist.entry(self).or_insert_with(|| rng.gen())
    }
}

#[derive(Clone, Copy, Debug)]
struct Ttvalue {
    depth: u32,
    value: i32,
    action: Action,
    bound: Ttbound,
}

impl Ttvalue {
    fn new(mut depth: u32, value: i32, action: Action, bound: Ttbound) -> Self {
        if value.abs() == i32::MAX {
            depth = u32::MAX;
        }
        Self { depth, value, action, bound }
    }
}

#[derive(Clone, Copy, Debug)]
enum Ttbound {
    Lower,
    Exact,
    Upper,
}

impl Ttbound {
    fn from_alpha_beta(value: i32, alpha: i32, beta: i32) -> Self {
        if value <= alpha {
            return Self::Upper;
        } else if value >= beta {
            return Self::Lower;
        }
        Self::Exact
    }
}

fn heuristic(game: &mut Game) -> i32 {
    let mut value = game.ndice[!game.turn as usize] as i32 - game.ndice[game.turn as usize] as i32;
    for (pos, die) in game.board.clone() {
        if game.is_free(pos) {
            value += (4 * (die.color == game.turn) as i32 - 2) * die.value as i32;
        }
    }
    value
}
