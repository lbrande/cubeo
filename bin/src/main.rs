use bin::{GameArea, Menu};
use gio::prelude::*;
use gtk::{
    prelude::*, Application, ApplicationBuilder, ApplicationWindow, Box, CssProvider, Orientation,
    StyleContext, STYLE_PROVIDER_PRIORITY_APPLICATION,
};
use std::rc::Rc;

const STYLE: &str = ".button-group-header { font-weight: bold; }";

fn main() {
    let application = ApplicationBuilder::new().build();
    application.connect_activate(setup);
    application.run(&[]);
}

fn setup(app: &Application) {
    let window = Rc::new(ApplicationWindow::new(app));
    setup_style(&window);
    window.set_resizable(false);

    let content = Box::new(Orientation::Horizontal, 0);
    window.add(&content);

    let game_area = Rc::new(GameArea::new());
    content.add(game_area.widget());

    let menu = Menu::new(Rc::clone(&window), Rc::clone(&game_area));
    content.add(menu.widget());

    setup_title_bar(Rc::clone(&window), &game_area);
    window.show_all();
}

fn setup_style(window: &Rc<ApplicationWindow>) {
    let provider = CssProvider::new();
    provider.load_from_data(STYLE.as_bytes()).unwrap();
    StyleContext::add_provider_for_screen(
        &window.get_display().get_default_screen(),
        &provider,
        STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

fn setup_title_bar(window: Rc<ApplicationWindow>, game_area: &Rc<GameArea>) {
    game_area.widget().connect_draw({
        let game_area = Rc::clone(game_area);
        move |_, _| {
            let players = format!("{} vs {}", game_area.players()[0], game_area.players()[1]);
            let game = match game_area.use_restrictive_rule_set() {
                true => "Restrictive rule set - Cubeo",
                false => "Open rule set - Cubeo",
            };
            window.set_title(&match game_area.winner() {
                Some(color) => format!("{} won - {} - {}", color, players, game),
                None => format!("{}'s turn - {} - {}", game_area.turn(), players, game),
            });
            Inhibit(false)
        }
    });
}
