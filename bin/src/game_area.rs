use crate::Player;
use cairo::Context;
use gdk::EventMask;
use glib::{MainContext, Receiver, Sender, PRIORITY_DEFAULT};
use gtk::{prelude::*, DrawingArea, Widget};
use lib::{ai::Minimax, Action, Color, Die, Game, Pos, MAX_NDICE};
use std::{
    cell::{Ref, RefCell, RefMut},
    f64::consts::PI,
    rc::Rc,
    sync::{Arc, Mutex},
    thread,
    time::{Duration, Instant},
};

const SQUARE_SIZE: f64 = 60.0;
const TOTAL_WIDTH: f64 = 12.0 * SQUARE_SIZE;
const TOTAL_HEIGHT: f64 = 15.0 * SQUARE_SIZE;
const DIE_MARGIN: f64 = 2.0;
const DIE_SIZE: f64 = SQUARE_SIZE - 2.0 * DIE_MARGIN;
const CORNER_SIZE: f64 = 6.0;

const BACKGROUND_COLOR: CairoColor = CairoColor::Rgb(0.15, 0.35, 0.15);
const SUPPLY_DELIMITER_COLOR: CairoColor = CairoColor::Rgb(0.35, 0.55, 0.15);
const RED_COLOR: CairoColor = CairoColor::Rgb(0.55, 0.15, 0.15);
const BLACK_COLOR: CairoColor = CairoColor::Rgb(0.15, 0.15, 0.15);
const DOT_COLOR: CairoColor = CairoColor::Rgb(0.85, 0.85, 0.85);
const BORDER_COLOR: CairoColor = CairoColor::Rgba(1.0, 1.0, 1.0, 0.2);
const TARGET_EMPTY_POS_COLOR: CairoColor = CairoColor::Rgba(0.0, 0.0, 0.0, 0.4);
const TARGET_DIE_COLOR: CairoColor = CairoColor::Rgb(0.85, 0.85, 0.85);
const SELECTED_DIE_COLOR: CairoColor = CairoColor::Rgb(0.85, 0.85, 0.15);

#[derive(Debug)]
pub struct GameArea {
    data: Rc<RefCell<Data>>,
    sender: Sender<(Action, Instant)>,
    widget: DrawingArea,
}

impl Default for GameArea {
    fn default() -> Self {
        Self::new()
    }
}

impl GameArea {
    pub fn new() -> Self {
        let (sender, receiver) = MainContext::channel(PRIORITY_DEFAULT);
        Self {
            data: Rc::new(RefCell::new(Data {
                game: Game::new(),
                players: [Player::Human; 2],
                selected_pos: None,
                start_time: Instant::now(),
                origin_x: 0.0,
                origin_y: 0.0,
            })),
            sender,
            widget: DrawingArea::new(),
        }
        .init(receiver)
    }

    fn init(self, receiver: Receiver<(Action, Instant)>) -> Self {
        self.widget.set_size_request(TOTAL_WIDTH as i32, TOTAL_HEIGHT as i32);
        update_origin(&mut self.data.borrow_mut(), 0.0, 0.5 * SQUARE_SIZE);
        self.attach_receiver(receiver);
        self.connect_draw();
        self.connect_button_press_event();
        self
    }

    fn attach_receiver(&self, receiver: Receiver<(Action, Instant)>) {
        let data = Rc::clone(&self.data);
        let sender = self.sender.clone();
        let widget = self.widget.clone();
        receiver.attach(None, move |(action, start_time)| {
            let mut data = data.borrow_mut();
            if start_time > data.start_time {
                data.game.perform_action(action).unwrap();
                notify_ai(&mut data, &sender);
                widget.queue_draw();
            }
            Continue(true)
        });
    }

    fn connect_draw(&self) {
        let data = Rc::clone(&self.data);
        self.widget.connect_draw(move |_, context| {
            let data = data.borrow();
            set_color(context, BACKGROUND_COLOR);
            context.paint();
            for (&pos, &die) in data.game.board() {
                draw_die(&data, context, pos, die);
            }
            if data.current_player() == Player::Human {
                if let Some(pos) = data.selected_pos {
                    target_die(&data, context, pos, SELECTED_DIE_COLOR);
                    for &action in data.game.actions() {
                        if action.from() == Some(pos) {
                            match action {
                                Action::Merge(_, to, _) => {
                                    target_die(&data, context, to, TARGET_DIE_COLOR);
                                }
                                Action::Move(_, to) => {
                                    target_pos(&data, context, to, TARGET_EMPTY_POS_COLOR);
                                }
                                Action::Add(_) | Action::None => {}
                            }
                        }
                    }
                } else {
                    for &action in data.game.actions() {
                        match action {
                            Action::Add(pos) => {
                                target_pos(&data, context, pos, TARGET_EMPTY_POS_COLOR);
                            }
                            Action::Merge(from, _, _) => {
                                target_die(&data, context, from, TARGET_DIE_COLOR);
                            }
                            Action::Move(from, _) => {
                                target_die(&data, context, from, TARGET_DIE_COLOR);
                            }
                            Action::None => {}
                        }
                    }
                }
            }
            draw_supply(context, data.game.ndice());
            Inhibit(false)
        });
    }

    fn connect_button_press_event(&self) {
        let data = Rc::clone(&self.data);
        let sender = self.sender.clone();
        self.widget.connect_button_press_event(move |canvas, event| {
            let mut data = data.borrow_mut();
            if data.current_player() == Player::Human {
                let x = ((event.get_position().0 - data.origin_x) / SQUARE_SIZE).floor() as i32;
                let y = ((data.origin_y - event.get_position().1) / SQUARE_SIZE).ceil() as i32;
                let pos = Pos(x, y);
                if let Some(from) = data.selected_pos {
                    let from_value = data.game.die_at(from).unwrap().value();
                    if let Some(action) = (data.game.if_legal(Action::Merge(from, pos, from_value)))
                        .or_else(|| data.game.if_legal(Action::Move(from, pos)))
                    {
                        data.game.perform_action(action).unwrap();
                    }
                    data.selected_pos = None;
                } else if let Some(action) = data.game.if_legal(Action::Add(pos)) {
                    data.game.perform_action(action).unwrap();
                    data.selected_pos = None;
                } else if data.game.actions().any(|action| action.from() == Some(pos)) {
                    data.selected_pos = Some(pos);
                }
                notify_ai(&mut data, &sender);
                canvas.queue_draw();
            }
            Inhibit(false)
        });
        self.widget.add_events(EventMask::BUTTON_PRESS_MASK);
    }

    pub fn new_game(&self, players: [Player; 2], use_restrictive_rule_set: bool) {
        let mut data = self.data.borrow_mut();
        data.game = match use_restrictive_rule_set {
            true => Game::with_restrictive_rule_set(),
            false => Game::new(),
        };
        data.players = players;
        update_origin(&mut data, 0.0, 0.5 * SQUARE_SIZE);
        notify_ai(&mut data, &self.sender);
        self.widget.queue_draw();
    }

    pub fn undo_action(&self) {
        let mut data = self.data.borrow_mut();
        data.game.undo_action().unwrap();
        if data.current_player() != Player::Human {
            data.game.undo_action().unwrap();
        }
        notify_ai(&mut data, &self.sender);
        self.widget.queue_draw();
    }

    pub fn center_game(&self) {
        let mut data = self.data.borrow_mut();
        let mut xmin = i32::MAX;
        let mut xmax = i32::MIN;
        let mut ymin = i32::MAX;
        let mut ymax = i32::MIN;
        for (&Pos(x, y), _) in data.game.board() {
            xmin = xmin.min(x);
            xmax = xmax.max(x);
            ymin = ymin.min(y);
            ymax = ymax.max(y);
        }
        let origin_x = 0.5 * SQUARE_SIZE * f64::from(xmin + xmax);
        let origin_y = 0.5 * SQUARE_SIZE * f64::from(ymin + ymax);
        update_origin(&mut data, origin_x, origin_y);
        self.widget.queue_draw();
    }

    pub fn turn(&self) -> Color {
        self.data.borrow().game.turn()
    }

    pub fn winner(&self) -> Option<Color> {
        self.data.borrow().game.winner()
    }

    pub fn use_restrictive_rule_set(&self) -> bool {
        self.data.borrow().game.use_restrictive_rule_set()
    }

    pub fn has_started(&self) -> bool {
        !self.data.borrow().game.history().is_empty()
    }

    pub fn players(&self) -> [Player; 2] {
        self.data.borrow().players
    }

    pub fn undo_available(&self) -> bool {
        let data = self.data.borrow();
        let history = data.game.history();
        !history.is_empty() && data.players[!data.game.turn() as usize] == Player::Human
            || (data.current_player() == Player::Human && history.len() > 1)
    }

    pub fn widget(&self) -> &impl IsA<Widget> {
        &self.widget
    }
}

#[derive(Debug)]
pub struct Data {
    game: Game,
    players: [Player; 2],
    selected_pos: Option<Pos>,
    start_time: Instant,
    origin_x: f64,
    origin_y: f64,
}

impl Data {
    fn current_player(&self) -> Player {
        self.players[self.game.turn() as usize]
    }
}

fn update_origin(data: &mut RefMut<Data>, origin_x: f64, origin_y: f64) {
    data.origin_x = 0.5 * (TOTAL_WIDTH - SQUARE_SIZE) - origin_x;
    data.origin_y = 0.5 * (TOTAL_HEIGHT - SQUARE_SIZE) + origin_y;
}

fn notify_ai(data: &mut RefMut<Data>, sender: &Sender<(Action, Instant)>) {
    data.start_time = Instant::now();
    if data.game.winner().is_none() {
        if let Player::Computer(dur) = data.current_player() {
            let game = data.game.clone();
            let sender = sender.clone();
            thread::spawn(move || {
                let start_time = Instant::now();
                let action = Arc::new(Mutex::new(Action::None));
                spawn_ai_worker(dur, game, Arc::clone(&action));
                thread::sleep(dur);
                sender.send((*action.lock().unwrap(), start_time)).unwrap();
            });
        }
    }
}

fn spawn_ai_worker(dur: Duration, game: Game, action: Arc<Mutex<Action>>) {
    thread::spawn(move || {
        let start_time = Instant::now();
        let mut minimax = Minimax::new();
        minimax.prepare_search(game);
        for depth in 1..u32::MAX {
            minimax.search(depth, -i32::MAX, i32::MAX);
            *action.lock().unwrap() = minimax.root_action();
            if minimax.is_finished() || start_time.elapsed() >= dur {
                break;
            }
        }
    });
}

fn draw_die(data: &Ref<Data>, context: &Context, Pos(x, y): Pos, die: Die) {
    let x = data.origin_x + SQUARE_SIZE * f64::from(x);
    let y = data.origin_y - SQUARE_SIZE * f64::from(y);
    draw_rectangle_and_dots(context, x, y, die);
}

fn target_pos(data: &Ref<Data>, context: &Context, Pos(x, y): Pos, color: CairoColor) {
    set_color(context, color);
    let x = data.origin_x + SQUARE_SIZE * f64::from(x);
    let y = data.origin_y - SQUARE_SIZE * f64::from(y);
    die_rectangle(context, x, y);
    context.fill();
}

fn target_die(data: &Ref<Data>, context: &Context, Pos(x, y): Pos, color: CairoColor) {
    set_color(context, color);
    let x = data.origin_x + SQUARE_SIZE * f64::from(x);
    let y = data.origin_y - SQUARE_SIZE * f64::from(y);
    die_rectangle(context, x, y);
    context.stroke();
}

fn draw_supply(context: &Context, ndice: [u32; 2]) {
    set_color(context, BACKGROUND_COLOR);
    context.rectangle(0.0, 0.0, TOTAL_WIDTH, 1.5 * SQUARE_SIZE);
    context.fill();
    context.rectangle(0.0, TOTAL_HEIGHT - 1.5 * SQUARE_SIZE, TOTAL_WIDTH, 1.5 * SQUARE_SIZE);
    context.fill();
    set_color(context, SUPPLY_DELIMITER_COLOR);
    context.set_line_width(4.0);
    context.move_to(0.0, 1.5 * SQUARE_SIZE - 2.0);
    context.line_to(TOTAL_WIDTH, 1.5 * SQUARE_SIZE - 2.0);
    context.stroke();
    context.move_to(0.0, TOTAL_HEIGHT - 1.5 * SQUARE_SIZE + 2.0);
    context.line_to(TOTAL_WIDTH, TOTAL_HEIGHT - 1.5 * SQUARE_SIZE + 2.0);
    context.stroke();
    context.set_line_width(2.0);
    for i in 0..MAX_NDICE - ndice[Color::Red as usize] {
        let x = 0.5 * TOTAL_WIDTH - SQUARE_SIZE * (2.5 - i as f64);
        let y = TOTAL_HEIGHT - 1.25 * SQUARE_SIZE;
        draw_rectangle_and_dots(context, x, y, Die::with_color(Color::Red));
    }
    for i in 0..MAX_NDICE - ndice[Color::Black as usize] {
        let x = 0.5 * TOTAL_WIDTH + SQUARE_SIZE * (1.5 - i as f64);
        let y = 0.25 * SQUARE_SIZE;
        draw_rectangle_and_dots(context, x, y, Die::with_color(Color::Black));
    }
}

fn draw_rectangle_and_dots(context: &Context, x: f64, y: f64, die: Die) {
    match die.color() {
        Color::Red => set_color(context, RED_COLOR),
        Color::Black => set_color(context, BLACK_COLOR),
    }
    die_rectangle(context, x, y);
    context.fill_preserve();
    set_color(context, BORDER_COLOR);
    context.stroke();
    draw_dots(context, x, y, die);
}

fn die_rectangle(context: &Context, x: f64, y: f64) {
    let x = x + DIE_MARGIN;
    let y = y + DIE_MARGIN;
    let xleft = x + CORNER_SIZE;
    let xright = x + DIE_SIZE - CORNER_SIZE;
    let ytop = y + CORNER_SIZE;
    let ybottom = y + DIE_SIZE - CORNER_SIZE;
    context.new_sub_path();
    context.arc(xleft, ytop, CORNER_SIZE, PI, 1.5 * PI);
    context.arc(xright, ytop, CORNER_SIZE, 1.5 * PI, 0.0);
    context.arc(xright, ybottom, CORNER_SIZE, 0.0, 0.5 * PI);
    context.arc(xleft, ybottom, CORNER_SIZE, 0.5 * PI, PI);
    context.close_path();
}

fn draw_dots(context: &Context, x: f64, y: f64, die: Die) {
    let x = x + DIE_MARGIN;
    let y = y + DIE_MARGIN;
    set_color(context, DOT_COLOR);
    match die.value() {
        1 => draw_dot(context, x, y, 0, 0),
        2 => {
            draw_dot(context, x, y, -1, 1);
            draw_dot(context, x, y, 1, -1);
        }
        3 => {
            draw_dot(context, x, y, 0, 0);
            draw_dot(context, x, y, -1, 1);
            draw_dot(context, x, y, 1, -1);
        }
        4 => {
            draw_dot(context, x, y, -1, 1);
            draw_dot(context, x, y, -1, -1);
            draw_dot(context, x, y, 1, 1);
            draw_dot(context, x, y, 1, -1);
        }
        5 => {
            draw_dot(context, x, y, 0, 0);
            draw_dot(context, x, y, -1, 1);
            draw_dot(context, x, y, -1, -1);
            draw_dot(context, x, y, 1, 1);
            draw_dot(context, x, y, 1, -1);
        }
        6 => {
            draw_dot(context, x, y, -1, 1);
            draw_dot(context, x, y, -1, -1);
            draw_dot(context, x, y, 1, 1);
            draw_dot(context, x, y, 1, -1);
            draw_dot(context, x, y, -1, 0);
            draw_dot(context, x, y, 1, 0);
        }
        _ => {
            let xc = x + 0.5 * DIE_SIZE;
            let yc = y + 0.5 * DIE_SIZE;
            context.arc(xc, yc, 0.4 * DIE_SIZE, 0.0, 2.0 * PI);
            context.fill();
        }
    }
}

fn draw_dot(context: &Context, x: f64, y: f64, xalign: i32, yalign: i32) {
    let xc = x + 0.5 * DIE_SIZE + 0.25 * DIE_SIZE * f64::from(xalign);
    let yc = y + 0.5 * DIE_SIZE + 0.25 * DIE_SIZE * f64::from(yalign);
    context.arc(xc, yc, 0.1 * DIE_SIZE, 0.0, 2.0 * PI);
    context.fill();
}

#[derive(Clone, Copy, Debug)]
enum CairoColor {
    Rgb(f64, f64, f64),
    Rgba(f64, f64, f64, f64),
}

fn set_color(context: &Context, color: CairoColor) {
    match color {
        CairoColor::Rgb(r, g, b) => context.set_source_rgb(r, g, b),
        CairoColor::Rgba(r, g, b, a) => context.set_source_rgba(r, g, b, a),
    }
}
