# Cubeo

A digital adaption of the abstract strategy game [Cubeo](https://boardgamegeek.com/boardgame/191916/cubeo).

## Features

- Play against a friend (hot seat)
- Play against the computer (freely adjustable difficulty level)
- Watch the computer play against itself
- Explore alternative outcomes with the undo functionality

### Rules

The original Cubeo rules had some ambiquities, thus different rule sets has been established.

This adaption allows the user to choose between two different rule sets:

- Open (recommended)
    1. Actions that require lifting dice into and out of the formation are allowed
    2. Merging does not require having 3 dice in play
- Restrictive (sanctioned by the designer, 25 % slower)
    1. Actions that would require lifting dice into or out of the formation are not allowed
    2. Merging requires having 3 dice in play

I strongly encourage everyone to give the Open rule set (which in my opinion is cleaner and leads to more variation) an honest chance.

Since the computer is capable of reaching a level of play where stalemates are quite common the following rule has been added to both rule sets: Moving is not allowed if the last 6 actions were moves. Note that this might cause a player to lose the game due to having no valid actions. The effect on gameplay is supposedly minimal.

## Installation

### Windows

1. Download [cubeo_0.2.0_win64.zip](/uploads/32e227e88bc7a053c589cbbcb80e6f81/cubeo_0.2.0_win64.zip) ([cubeo_0.2.0_win32.zip](/uploads/b9aaa1a4cb3029efb71796ec9d6a3583/cubeo_0.2.0_win32.zip) for 32-bit Windows) and extract its contents to a suitable location
2. Open `cubeo_win64\bin` (`cubeo_win32\bin` for 32-bit Windows) in "File Explorer"
3. Right-click `Cubeo.exe` and select "Create shortcut"
4. Move the newly created shortcut to your desktop

You can now launch the game by double-clicking the shortcut on your desktop.

### Linux (Ubuntu or Linux Mint)

1. Download [cubeo_0.2.0_amd64.deb](/uploads/b22b9d97d33e401c1ff029db3887e4ea/cubeo_0.2.0_amd64.deb)
2. Choose open with "Software Install"
3. Press "Install", then wait for the installation to complete

You can now launch the game from the "Applications" menu or by opening a terminal and entering the command `cubeo`.

## Report a bug / Suggest a feature

If you want to report a bug or suggest a feature you are free to [open an issue](https://gitlab.com/lbrande/cubeo/-/issues) (follow the link then click "New issue", if you're not logged in or don't have an account click "Register / Sign In" first). Make sure to provide a meaningful title as well as a description with all the relevant details.

## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
