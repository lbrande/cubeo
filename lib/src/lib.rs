#![forbid(unsafe_code)]
use hash::RandomHashSet;
use rustc_hash::{FxHashMap, FxHashSet};
use smallvec::{smallvec, SmallVec};
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    hash::BuildHasherDefault,
    ops::Not,
};

pub mod ai;
mod hash;

pub const MAX_NDICE: u32 = 6;
pub const MAX_VALUE: u32 = 6;
pub const MAX_NCONSECUTIVE_MOVES: usize = 6;

#[derive(Clone, Debug)]
pub struct Game {
    board: FxHashMap<Pos, Die>,
    ndice: [u32; 2],
    is_free: FxHashMap<Pos, bool>,
    can_slide: FxHashMap<Pos, bool>,
    actions: RandomHashSet<Action>,
    history: Vec<Action>,
    turn: Color,
    winner: Option<Color>,
    use_restrictive_rule_set: bool,
}

impl Default for Game {
    fn default() -> Self {
        Self::new()
    }
}

impl Game {
    pub fn new() -> Self {
        Self {
            board: FxHashMap::default(),
            ndice: [1; 2],
            is_free: FxHashMap::default(),
            can_slide: FxHashMap::default(),
            actions: RandomHashSet::default(),
            history: Vec::new(),
            turn: Color::Red,
            winner: None,
            use_restrictive_rule_set: false,
        }
        .init()
    }

    pub fn with_restrictive_rule_set() -> Self {
        Self { use_restrictive_rule_set: true, ..Self::default() }
    }

    fn init(mut self) -> Self {
        self.board.insert(Pos(0, 1), Die::with_color(Color::Black));
        self.board.insert(Pos(0, 0), Die::with_color(Color::Red));
        self.generate_actions();
        self
    }

    pub fn perform_action(&mut self, action: Action) -> Result<(), PerformActionError> {
        let &action = self.actions.get(&action).ok_or(PerformActionError)?;
        self.actions.clear();
        self.winner = action.perform(self);
        if self.winner.is_none() {
            self.generate_actions();
            self.winner = self.actions.is_empty().then(|| !self.turn);
        };
        Ok(())
    }

    pub fn undo_action(&mut self) -> Result<Action, UndoActionError> {
        let action = self.history.pop().ok_or(UndoActionError)?;
        action.undo(self);
        self.actions.clear();
        self.generate_actions();
        self.winner = None;
        Ok(action)
    }

    pub fn board(&self) -> impl Iterator<Item = (&Pos, &Die)> {
        self.board.iter()
    }

    pub fn ndice(&self) -> [u32; 2] {
        self.ndice
    }

    pub fn actions(&self) -> impl Iterator<Item = &Action> {
        self.actions.iter()
    }

    pub fn history(&self) -> &[Action] {
        &self.history
    }

    pub fn turn(&self) -> Color {
        self.turn
    }

    pub fn winner(&self) -> Option<Color> {
        self.winner
    }

    pub fn use_restrictive_rule_set(&self) -> bool {
        self.use_restrictive_rule_set
    }

    pub fn die_at(&self, pos: Pos) -> Option<Die> {
        self.board.get(&pos).copied()
    }

    pub fn if_legal(&self, action: Action) -> Option<Action> {
        self.actions.get(&action).copied()
    }

    pub fn nconsecutive_moves(&self) -> usize {
        (self.history.iter().rev())
            .position(|action| !matches!(action, Action::Move(_, _)))
            .unwrap_or(self.history.len())
    }

    fn generate_actions(&mut self) {
        self.is_free.clear();
        self.can_slide.clear();
        for (from, die) in self.board.clone() {
            if die.color == self.turn {
                if self.ndice[self.turn as usize] < MAX_NDICE {
                    for pos in from.adjacents() {
                        if self.can_add(pos) {
                            self.actions.insert(Action::Add(pos));
                        }
                    }
                }
                if !self.use_restrictive_rule_set || self.ndice[self.turn as usize] > 2 {
                    for to in from.adjacents() {
                        if self.can_merge(from, to) {
                            self.actions.insert(Action::Merge(from, to, die.value));
                        }
                    }
                }
                if self.nconsecutive_moves() < MAX_NCONSECUTIVE_MOVES && self.is_free(from) {
                    let is_empty = |pos| pos == from || self.is_empty(pos);
                    let mut to_vec: SmallVec<[_; 16]> = smallvec![from];
                    for _ in 0..die.value {
                        to_vec = to_vec.iter().flat_map(|&pos| self.steps(pos, is_empty)).collect();
                    }
                    for to in to_vec {
                        if to != from {
                            self.actions.insert(Action::Move(from, to));
                        }
                    }
                }
            }
        }
    }

    fn can_add(&mut self, pos: Pos) -> bool {
        self.is_empty(pos)
            && !pos.adjacents().any(|pos| self.has_color(pos, !self.turn))
            && (!self.use_restrictive_rule_set || self.can_slide(pos))
    }

    fn can_merge(&mut self, from: Pos, to: Pos) -> bool {
        self.has_color(to, self.turn)
            && self.is_free(from)
            && (!self.use_restrictive_rule_set || self.can_slide(from))
    }

    fn is_free(&mut self, pos: Pos) -> bool {
        if let Some(&is_free) = self.is_free.get(&pos) {
            return is_free;
        }
        let start = pos.adjacents().find(|&pos| !self.is_empty(pos)).unwrap();
        let mut stack: SmallVec<[_; 16]> = smallvec![start];
        let hasher = BuildHasherDefault::default();
        let mut visited = FxHashSet::with_capacity_and_hasher(self.board.len(), hasher);
        visited.insert(pos);
        while let Some(pos) = stack.pop() {
            visited.insert(pos);
            for pos in pos.adjacents() {
                if !visited.contains(&pos) && !self.is_empty(pos) {
                    stack.push(pos);
                }
            }
        }
        let is_free = visited.len() == self.board.len();
        self.is_free.insert(pos, is_free);
        is_free
    }

    fn can_slide(&mut self, pos: Pos) -> bool {
        if let Some(&can_slide) = self.can_slide.get(&pos) {
            return can_slide;
        }
        let mut can_slide = false;
        let mut stack: SmallVec<[_; 16]> = smallvec![pos];
        let mut visited = FxHashSet::default();
        while let Some(pos) = stack.pop() {
            if pos.adjacents().filter(|&pos| self.is_empty(pos)).count() > 2 {
                can_slide = true;
                break;
            }
            visited.insert(pos);
            for pos in pos.adjacents() {
                if !visited.contains(&pos) && self.is_empty(pos) {
                    stack.push(pos);
                }
            }
        }
        self.can_slide.insert(pos, can_slide);
        can_slide
    }

    fn steps(&self, Pos(x, y): Pos, is_empty: impl Fn(Pos) -> bool) -> impl Iterator<Item = Pos> {
        let mut steps: SmallVec<[_; 4]> = SmallVec::new();
        for dx in -1..=1 {
            for dy in -1..=1 {
                let pos = Pos(x + dx, y + dy);
                if dx.abs() + dy.abs() == 2 {
                    let s1 = Pos(x + dx, y);
                    let s2 = Pos(x, y + dy);
                    if is_empty(pos) && is_empty(s1) ^ is_empty(s2) {
                        steps.push(pos);
                    }
                } else if dx != 0 {
                    let c1 = Pos(x + dx, y - 1);
                    let s1 = Pos(x, y - 1);
                    let c2 = Pos(x + dx, y + 1);
                    let s2 = Pos(x, y + 1);
                    if is_empty(pos)
                        && ((!is_empty(c1) && !is_empty(s1)) || (!is_empty(c2) && !is_empty(s2)))
                    {
                        steps.push(pos);
                    }
                } else if dy != 0 {
                    let c1 = Pos(x - 1, y + dy);
                    let s1 = Pos(x - 1, y);
                    let c2 = Pos(x + 1, y + dy);
                    let s2 = Pos(x + 1, y);
                    if is_empty(pos)
                        && ((!is_empty(c1) && !is_empty(s1)) || (!is_empty(c2) && !is_empty(s2)))
                    {
                        steps.push(pos);
                    }
                }
            }
        }
        steps.into_iter()
    }

    fn is_empty(&self, pos: Pos) -> bool {
        !self.board.contains_key(&pos)
    }

    fn has_color(&self, pos: Pos, color: Color) -> bool {
        self.board.get(&pos).filter(|die| die.color == color).is_some()
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum Action {
    Add(Pos),
    Merge(Pos, Pos, u32),
    Move(Pos, Pos),
    None,
}

impl Action {
    pub fn from(self) -> Option<Pos> {
        match self {
            Self::Merge(from, _, _) => Some(from),
            Self::Move(from, _) => Some(from),
            Self::Add(_) | Self::None => None,
        }
    }

    fn perform(self, game: &mut Game) -> Option<Color> {
        let mut winner = None;
        match self {
            Self::Add(pos) => {
                game.board.insert(pos, Die::with_color(game.turn));
                game.ndice[game.turn as usize] += 1;
            }
            Self::Merge(from, to, from_value) => {
                game.board.remove(&from).unwrap();
                game.ndice[game.turn as usize] -= 1;
                let to_die = game.board.get_mut(&to).unwrap();
                to_die.value += from_value;
                winner = Some(game.turn).filter(|_| to_die.value > MAX_VALUE);
            }
            Self::Move(from, to) => {
                let die = game.board.remove(&from).unwrap();
                game.board.insert(to, die);
            }
            Self::None => {}
        }
        game.history.push(self);
        game.turn = !game.turn;
        winner
    }

    fn undo(self, game: &mut Game) {
        game.turn = !game.turn;
        match self {
            Self::Add(pos) => {
                game.board.remove(&pos);
                game.ndice[game.turn as usize] -= 1;
            }
            Self::Merge(from, to, from_value) => {
                game.board.insert(from, Die { color: game.turn, value: from_value });
                game.ndice[game.turn as usize] += 1;
                game.board.get_mut(&to).unwrap().value -= from_value;
            }
            Self::Move(from, to) => {
                let die = game.board.remove(&to).unwrap();
                game.board.insert(from, die);
            }
            Self::None => {}
        }
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct PerformActionError;

impl Display for PerformActionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "PerformActionError: action is illegal")
    }
}

impl Error for PerformActionError {}

#[derive(Clone, Copy, Debug, Default)]
pub struct UndoActionError;

impl Display for UndoActionError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "UndoActionError: no action to undo")
    }
}

impl Error for UndoActionError {}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub struct Die {
    color: Color,
    value: u32,
}

impl Die {
    pub fn with_color(color: Color) -> Self {
        Self { color, value: 1 }
    }

    pub fn color(self) -> Color {
        self.color
    }

    pub fn value(self) -> u32 {
        self.value
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum Color {
    Red,
    Black,
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Red => write!(f, "Red"),
            Self::Black => write!(f, "Black"),
        }
    }
}

impl Not for Color {
    type Output = Color;

    fn not(self) -> Self::Output {
        match self {
            Self::Red => Self::Black,
            Self::Black => Self::Red,
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Hash)]
pub struct Pos(pub i32, pub i32);

impl Pos {
    fn adjacents(self) -> impl Iterator<Item = Self> {
        let Self(x, y) = self;
        [Self(x - 1, y), Self(x, y - 1), Self(x, y + 1), Self(x + 1, y)].into_iter()
    }
}
