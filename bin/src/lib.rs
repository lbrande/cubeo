#![forbid(unsafe_code)]
use std::{
    fmt::{self, Display, Formatter},
    time::Duration,
};

mod game_area;
mod menu;

pub use game_area::GameArea;
pub use menu::Menu;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Player {
    Human,
    Computer(Duration),
}

impl Display for Player {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Human => write!(f, "Human"),
            Self::Computer(_) => write!(f, "Computer"),
        }
    }
}
